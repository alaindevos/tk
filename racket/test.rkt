#lang typed/racket
(require typed/racket/gui
         typed/racket/class)
(define window
  (new frame%
       (label "Hello World!")
       (width 100)
       (height 100)))
(send window show #t)
