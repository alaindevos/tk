import std.stdio;
import tkd.tkdapplication;                               // Import Tkd.
import std.conv;

class MyTkdApplication : TkdApplication{     // Extend TkdApplication.
    Frame frame;
    Entry entry;
    Button button;
    Label label;
    Button exitButton;

    private void exitCommand(CommandArgs args){  // Create a callback.
        this.exit();                                                            // Exit the application.
    }

    private void buttonPressed(CommandArgs args){  // Create a callback.
        string fs=entry.getValue();
        float f=to!float(fs);
        float c=(f-32.0)*5.0/9.0;
        string cs=to!string(c);
        label.setText(cs);
    }

    override protected void initInterface(){             // Initialise user interface.
   
        frame = new Frame(2, ReliefStyle.groove)         // Create a frame.
            .pack(10);                                   // Place the frame.

        entry = new Entry(frame)   // Create a entry.
            .pack(10);                       // Place the entry.

        button = new Button(frame, "Convert ")   // Create a button.
            .setCommand(&this.buttonPressed)    // Use the callback.
            .pack(10);                                   // Place the button.

        label = new Label(frame, "____________")  // Create a label.
            .pack(10);                                   // Place the label.

        exitButton = new Button(frame, "Exit")    // Create a button.
            .setCommand(&this.exitCommand)    // Use the callback.
            .pack(10);                                   // Place the button.
    }
}

void main(string[] args){
    auto app = new MyTkdApplication(); // Create the application.
    app.run();                                           // Run the application.
}

